/*
* RNAseq pipeline
* cutadapt :
* Imputs : fastq files
* Output : fastq files
*/

/*                      Illumina adaptor removal                             */

/*
* for paired-end data
*/

params.fastq = "/home/jclaud01/Nextflow/nextflow/data/fastq/*_R{1,2}.fastq"

log.info "fastq files : ${params.fastq}"

Channel
  .fromFilePairs( params.fastq )
  .ifEmpty { error "Cannot find any fastq files matching: ${params.fastq}" }
  .set { fastq_files }

process adaptor_removal {
  tag "$pair_id"
  publishDir "results/fastq/adaptor_removal/", mode: 'copy'

  input:
  set pair_id, file(reads) from fastq_files

  output:
  file "*_cut_R{1,2}.fastq.gz" into fastq_files_cut

  script:
  """
  cutadapt -a AGATCGGAAGAG -g CTCTTCCGATCT -A AGATCGGAAGAG -G CTCTTCCGATCT \
  -o ${pair_id}_cut_R1.fastq.gz -p ${pair_id}_cut_R2.fastq.gz \
  ${reads[0]} ${reads[1]} > ${pair_id}_report.txt
  """
}


/*        quality trimming                                     */
/* for paired-end data */

process trimming {
  tag "$pair_id2"
  publishDir "results/fastq/trimming/", mode: 'copy'

  input:
  set pair_id2, file(reads) from fastq_files_cut

  output:
  file "*_trim_R{1,2}.fastq.gz" into fastq_files_trim

  script:
  """
  cutadapt -q 20,20 \
  -o ${pair_id}_trim_R1.fastq.gz -p ${pair_id}_trim_R2.fastq.gz \
  ${reads[0]} ${reads[1]} > ${pair_id}_report.txt
  """
}


/*          urqt             quality trimming                                     */
/* for paired-end data */


process trimming2 {
  tag "${pair_id3}"
  cpus 4
  publishDir "results/fastq/trimming2/", mode: 'copy'

  input:
  set pair_id3, file(reads) from fastq_files_trim

  output:
  file "*_trim2_R{1,2}.fastq.gz" into fastq_files_trim2

  script:
"""
UrQt --t 20 --m ${task.cpus} --gz \
--in ${reads[0]} --inpair ${reads[1]} \
--out ${reads[0].baseName}_trim_R1.fastq.gz --outpair ${reads[1].baseName}_trim_R2.fastq.gz \
> ${reads[0].baseName}_trimming_report.txt
"""
}


